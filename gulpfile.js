
const gulp = require('gulp');
const realFavicon = require ('gulp-real-favicon');
const fs = require('fs');

const SITE = {
  name: 'zimacorp.space',
  source: 'source.png',
  dir: 'icons/',
  color: '#f2b705',
  themeColor: '#590202'
}

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
	realFavicon.generateFavicon({
		masterPicture: SITE.source,
		dest: SITE.dir,
		iconsPath: SITE.dir,
		design: {
			ios: {
				pictureAspect: 'noChange',
				assets: {
					ios6AndPriorIcons: true,
					ios7AndLaterIcons: true,
					precomposedIcons: true,
					declareOnlyDefaultIcon: true
				},
				appName: SITE.name
			},
			desktopBrowser: {
				design: 'raw'
			},
			windows: {
				pictureAspect: 'noChange',
				backgroundColor: SITE.color,
				onConflict: 'override',
				assets: {
					windows80Ie10Tile: true,
					windows10Ie11EdgeTiles: {
						small: true,
						medium: true,
						big: true,
						rectangle: true
					}
				},
				appName: SITE.name
			},
			androidChrome: {
				pictureAspect: 'noChange',
				themeColor: SITE.themeColor,
				manifest: {
					name: SITE.name,
					display: 'standalone',
					orientation: 'notSet',
					onConflict: 'override',
					declared: true
				},
				assets: {
					legacyIcon: true,
					lowResolutionIcons: true
				}
			},
			safariPinnedTab: {
				pictureAspect: 'blackAndWhite',
				threshold: 93.75,
				themeColor: SITE.themeColor
			}
		},
		settings: {
			scalingAlgorithm: 'Mitchell',
			errorOnImageTooSmall: true,
			readmeFile: true,
			htmlCodeFile: true,
			usePathAsIs: false
		},
		markupFile: SITE.dir + '/faviconData.json'
	}, function() {
		done();
	});
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
	var currentVersion = JSON.parse(fs.readFileSync(SITE.dir + '/faviconData.json')).version;
	realFavicon.checkForUpdates(currentVersion, function(err) {
		if (err) {
			throw err;
		}
	});
});
